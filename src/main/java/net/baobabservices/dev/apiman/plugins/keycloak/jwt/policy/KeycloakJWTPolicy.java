/*
 * Copyright 2015 JBoss Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.baobabservices.dev.apiman.plugins.keycloak.jwt.policy;

import io.apiman.gateway.engine.beans.ApiRequest;
import io.apiman.gateway.engine.beans.ApiResponse;
import io.apiman.gateway.engine.beans.exceptions.ConfigurationParseException;
import io.apiman.gateway.engine.policy.IPolicy;
import io.apiman.gateway.engine.policy.IPolicyChain;
import io.apiman.gateway.engine.policy.IPolicyContext;
import io.apiman.gateway.engine.beans.PolicyFailure;
import io.apiman.gateway.engine.beans.PolicyFailureType;
import io.apiman.gateway.engine.components.IPolicyFailureFactoryComponent;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import java.util.Optional;
import jwt.auth.impl.providers.keycloak.SigningKeyResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A policy that does nothing. But hey, at least it never fails!
 *
 * @author eric.wittmann@redhat.com
 */
public class KeycloakJWTPolicy implements IPolicy {

    private static final String AUTHORIZATION_KEY = "Authorization"; //$NON-NLS-1$
    private static final String ACCESS_TOKEN_QUERY_KEY = "access_token"; //$NON-NLS-1$
    private static final String BEARER = "bearer "; //$NON-NLS-1$
    private static final SigningKeyResolver resolver = new SigningKeyResolver();

    private static final int AUTH_NO_TRANSPORT_SECURITY = 12003;
    private static final int AUTH_VERIFICATION_ERROR = 12004;
    private static final int AUTH_NOT_PROVIDED = 12005;
    private static final int AUTH_JWT_EXPIRED = 12006;
    private static final int AUTH_JWT_MALFORMED = 12007;
    private static final int AUTH_JWT_SIGNATURE_EXCEPTION = 12008;
    private static final int AUTH_JWT_CLAIM_FAILURE = 12009;
    private static final int AUTH_JWT_PREMATURE = 12010;
    private static final int AUTH_JWT_UNSUPPORTED_JWT = 12011;

    /**
     * Constructor.
     */
    public KeycloakJWTPolicy() {
    }

    private static final Logger logger = LogManager.getLogger(KeycloakJWTPolicy.class);

    public static void LogError(String msg, Throwable e) {
        logger.error(msg, e);
    }

    public static void LogInfo(String msg) {
        logger.info(msg);
    }

    /**
     * @see
     * io.apiman.gateway.engine.policy.IPolicy#parseConfiguration(java.lang.String)
     */
    @Override
    public Object parseConfiguration(String jsonConfiguration) throws ConfigurationParseException {
        return null;
    }

    /**
     * @see
     * io.apiman.gateway.engine.policy.IPolicy#apply(io.apiman.gateway.engine.beans.ApiRequest,
     * io.apiman.gateway.engine.policy.IPolicyContext, java.lang.Object,
     * io.apiman.gateway.engine.policy.IPolicyChain)
     */
    @Override
    public void apply(ApiRequest request, IPolicyContext context, Object config, IPolicyChain<ApiRequest> chain) {
        String jwt = Optional.ofNullable(request.getHeaders().get(AUTHORIZATION_KEY))
                // If seems to be bearer token
                .filter(e -> e.toLowerCase().startsWith(BEARER))
                // Get out token value
                .map(e -> e.substring(BEARER.length()))
                // Otherwise attempt to get from the access_token query param
                .orElse(request.getQueryParams().get(ACCESS_TOKEN_QUERY_KEY));

        if (jwt == null || !authenticateToken(jwt, request.getUrl())) {
            PolicyFailure pf = createAuthenticationPolicyFailure(context, AUTH_VERIFICATION_ERROR, "Request Not Authenticated");
            chain.doFailure(pf);
            return;
        }
        chain.doApply(request);
    }

    public boolean authenticateToken(String token, String uri) {
        boolean isAuthenticated = false;

        try {
            getJWT(token);
            isAuthenticated = true;

        } catch (Exception e) {

            LogInfo("Exception authenticating  " + uri);
            LogError("An error occurs: ", e);
        }

        return isAuthenticated;
    }

    /**
     * @see
     * io.apiman.gateway.engine.policy.IPolicy#apply(io.apiman.gateway.engine.beans.ApiResponse,
     * io.apiman.gateway.engine.policy.IPolicyContext, java.lang.Object,
     * io.apiman.gateway.engine.policy.IPolicyChain)
     */
    @Override
    public void apply(ApiResponse response, IPolicyContext context, Object config, IPolicyChain<ApiResponse> chain) {
        chain.doApply(response);
    }

    private PolicyFailure createAuthenticationPolicyFailure(IPolicyContext context, int failureCode,
            String message) {
        PolicyFailure pf = getFailureFactory(context).createFailure(PolicyFailureType.Authentication,
                failureCode, message);
        pf.setResponseCode(HTTP_UNAUTHORIZED);
        return pf;
    }

    private IPolicyFailureFactoryComponent getFailureFactory(IPolicyContext context) {
        return context.getComponent(IPolicyFailureFactoryComponent.class);
    }

    public Jws<Claims> getJWT(String claimsJwt) {

        Jws<Claims> jwt = Jwts.parser().setSigningKeyResolver(resolver).parseClaimsJws(claimsJwt);
        //LogInfo(jwt.getBody().getIssuer());
        return jwt;
    }

}

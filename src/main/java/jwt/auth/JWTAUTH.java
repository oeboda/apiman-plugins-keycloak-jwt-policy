/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jwt.auth;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import jwt.auth.impl.providers.keycloak.KeyCloakImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ADMIN
 */
public class JWTAUTH {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  {
        try {
            // TODO code application logic here
            JWTAUTH service = new JWTAUTH();
            int service_port = 8085;
            AuthRequestHandler h = service.new AuthRequestHandler();
            HttpServer httpServer = HttpServer.create(new InetSocketAddress(service_port), 2000);
            httpServer.createContext("/extauth", h);
            httpServer.start();
            LogInfo("JWT AUTH Service started .. listening on port " + service_port + " \n");
        } catch (IOException ex) {
            LogError("Error occured while starting the JWT AUTH service ", ex);
        }
    }

    private static final Logger logger = LogManager.getLogger(JWTAUTH.class);

    public static void LogError(String msg, Throwable e) {
        logger.error(msg, e);
    }

    public static void LogInfo(String msg) {
        logger.info(msg);
    }

    private AuthService auth_service = new KeyCloakImpl();

    public class AuthRequestHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange exchange) throws IOException {

            String response = "Request Authenticated";
            int response_code = 200;

            if (!auth_service.authenticateRequest(exchange.getRequestURI(), exchange.getRequestHeaders())) {
                response = "Request Not Authenticated";
                response_code = 401;
            }

            exchange.sendResponseHeaders(response_code, response.length());
            try (OutputStream os = exchange.getResponseBody()) {
                os.write(response.getBytes());
            }

        }
    }

}

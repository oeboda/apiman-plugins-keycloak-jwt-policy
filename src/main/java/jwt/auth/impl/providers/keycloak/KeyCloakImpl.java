/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jwt.auth.impl.providers.keycloak;

import com.sun.net.httpserver.Headers;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import java.net.URI;
import jwt.auth.AuthService;
import static jwt.auth.JWTAUTH.LogError;
import static jwt.auth.JWTAUTH.LogInfo;

/**
 *
 * @author ADMIN
 */
public class KeyCloakImpl implements AuthService {

    SigningKeyResolver resolver = new SigningKeyResolver();

    @Override
    public boolean authenticateRequest(URI uri, Headers h) {
        boolean isAuthenticated = false;

        String authorization = h.getFirst("Authorization");
        String token = authorization.replaceFirst("Bearer\\s*", "");
        try {
            getJWT(token);
            isAuthenticated = true;

        } catch (Exception e) {

            LogInfo("Exception authenticating  " + uri.toString());
            LogError("An error occurs: ", e);
        }

        return isAuthenticated;
    }
    
    public Jws<Claims> getJWT(String claimsJwt) {
        
        Jws<Claims> jwt = Jwts.parser().setSigningKeyResolver(resolver).parseClaimsJws(claimsJwt);
        //LogInfo(jwt.getBody().getIssuer());
        return jwt;
    }

}

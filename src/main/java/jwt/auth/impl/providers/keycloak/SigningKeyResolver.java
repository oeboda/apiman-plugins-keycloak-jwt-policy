/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jwt.auth.impl.providers.keycloak;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SigningKeyResolverAdapter;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import static jwt.auth.JWTAUTH.LogError;
import static jwt.auth.JWTAUTH.LogInfo;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ADMIN
 */
public class SigningKeyResolver extends SigningKeyResolverAdapter {

    private final Map<String, PublicKey> publickeys = new ConcurrentHashMap<>();

    @Override
    public Key resolveSigningKey(JwsHeader jwsHeader, Claims claims) {

        //inspect the header or claims, lookup and return the signing key
        String keyId = jwsHeader.getKeyId(); //or any other field that you need to inspect

        Key key = publickeys.get(keyId); //implement me
        if (key == null) {
            synchronized (keyId) {
                key = publickeys.get(keyId); //implement me
                if (key == null) {
                    String issuer = claims.getIssuer();
                    String jwks_uri = issuer + "/protocol/openid-connect/certs";
                    try {

                        String keys_payload_text = sendHTTPGETRequest(jwks_uri, "application/json");
                        JSONObject keys_payload = new JSONObject(keys_payload_text);
                        JSONArray keys = keys_payload.getJSONArray("keys");

                        keys.forEach((k) -> {
                            JSONObject key_info = (JSONObject) k;
                            try {
                                publickeys.put(key_info.getString("kid"), buildRSAPublicKey(key_info));
                            } catch (InvalidKeySpecException ex) {
                                LogInfo("Exception getting keys from " + jwks_uri);
                                LogError("An InvalidKeySpecException error occurs: ", ex);
                            } catch (NoSuchAlgorithmException ex) {
                                LogInfo("Exception getting keys from " + jwks_uri);
                                LogError("A NoSuchAlgorithmException error occurs: ", ex);
                            }
                        });
                        key = publickeys.get(keyId);

                    } catch (Exception ex) {
                        LogInfo("Exception getting keys from " + jwks_uri);
                        LogError("An error occurs: ", ex);
                    }
                }
            }

        }

        return key;
    }

    private PublicKey buildRSAPublicKey(JSONObject keyJson) throws InvalidKeySpecException, NoSuchAlgorithmException {
        String n = keyJson.getString("n");
        String e = keyJson.getString("e");
        String kty = keyJson.getString("kty");
//        System.out.println(n);
//        System.out.println(e);
//        System.out.println(kty);
        BigInteger modulus = new BigInteger(1, org.keycloak.common.util.Base64Url.decode(n));
        BigInteger publicExponent = new BigInteger(1, org.keycloak.common.util.Base64Url.decode(e));
        return KeyFactory.getInstance(kty).generatePublic(new RSAPublicKeySpec(modulus, publicExponent));
    }

    public static String sendHTTPJSONPOSTRequest(String req, String url) throws Exception {
        String response = null;
        int timeout = 20;
        try (CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(RequestConfig.custom().
                setConnectTimeout(timeout * 1000).
                setConnectionRequestTimeout(timeout * 1000).
                setSocketTimeout(timeout * 1000).setCookieSpec(CookieSpecs.STANDARD).build()).build();) {
            CookieStore cookieStore = new BasicCookieStore();
            HttpContext httpContext = new BasicHttpContext();
            httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

//            HttpPost login = new HttpPost(AUTHREQURL);
            StringEntity params = null; //new StringEntity(AUTHREQ);
//            login.addHeader("content-type", "application/json");
//            login.addHeader("Connection", "keep-alive");
//            login.setEntity(params);

            HttpPost request = new HttpPost(url);
            params = new StringEntity(req);
            request.addHeader("content-type", "application/json");
            request.setEntity(params);

//            HttpResponse result1 = httpClient.execute(login, httpContext);
//            response = EntityUtils.toString(result1.getEntity(), "UTF-8");
            HttpResponse result2 = httpClient.execute(request, httpContext);

            response = EntityUtils.toString(result2.getEntity(), "UTF-8");

        }

        return response;
    }

    public static String sendHTTPGETRequest(String url, String contextType) throws Exception {
        String response = null;
        int timeout = 20;
        try (CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(RequestConfig.custom().
                setConnectTimeout(timeout * 1000).
                setConnectionRequestTimeout(timeout * 1000).
                setSocketTimeout(timeout * 1000).setCookieSpec(CookieSpecs.STANDARD).build()).build();) {
            CookieStore cookieStore = new BasicCookieStore();
            HttpContext httpContext = new BasicHttpContext();
            httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
            HttpGet request = new HttpGet(url);

            request.addHeader("Accept", contextType);

            HttpResponse result2 = httpClient.execute(request, httpContext);

            response = EntityUtils.toString(result2.getEntity(), "UTF-8");

        }

        return response;
    }
}
